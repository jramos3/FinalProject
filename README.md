# FinalProject

## Intructions

Run server.py by being in the server directory. *You will have to change the path of the dorm.dat file

```
python3 server.py
```
The server will be live on port # 51023

The site will be live on: [https://jramos3.gitlab.io/FinalProject/frontend/index.html](https://jramos3.gitlab.io/FinalProject/frontend/index.html)

Run test by being in the same directory and running:

```
python3 test_api.py
python3 test_ws.py
```

## Links 

[presentation](https://youtu.be/1k6aFbRtnbQ)

[slides](https://drive.google.com/file/d/1XSQc7ZA77wD2fBcTUrWO8EKwPjQWB4Xt/view?usp=sharing)

[site demo](https://youtu.be/PBmGtu4IYh8)

[code walkthrough](https://youtu.be/KmKtjOrjobE)

## Data Description 

###### dorm.dat

For this project, I decided to use the same data from HW-08 with nwhalen2. We created a data file with information on the dorms at Notre Dame. It was manually typed and separated by a “--” delimiter. It is in JSON format, with index numbers as keys to sub-dictionaries with the dorm’s characteristics of name, year established, sex housed, quad located, and mascot. For instance, the first dorm described is Alumni, of key 1 with the following layout: {‘result’: ‘success’, ‘1’: {‘name’: ‘Alumni’, ‘year’: 1931, ‘gender’: ‘Male’, ‘quad’: ‘South’, ‘mascot’: ‘Dawgs’}, ‘2’: {‘name’: ‘Badin’... }... }. For my project, I created a website that pulls information on a Notre Dame dorm based on the dorm and keys requested through buttons. 

## OO API

###### dorm_library.py

The library created is one class created for the dorm database. Inside of the class, there are functions for handling the data.dat file. The following functions are part of the _dorm_database class. 

load_dorms() - extracts the data as lists based on their key category. It also places the lists in a dictionary for later manipulation. 

get_dorms() - returns all of the information received from load forms. 

get_dorm() - returns the information from one dorm by using the dorm’s id. 

set_dorm() - adds a new dorm by taking the id and defining its other categories.

delete_dorm() - removes a dorm’s data from the database by taking in the id 

###### test_api.py

Tests the functionality of the event handlers as defined in the dorm controller. It performs tests by checking what the server outputs is the expected data. For example, the test_dorm_get unit test uses two requests.get methods to confirm the functionality of the GET_KEY command after resetting the data perhaps altered from other tests, first with the dorm with id = 11, or Fisher Hall and using assertEqual() to affirm that the dorm’s information that was retrieved by the get_dorm function in dorm_library.py was equal to what was displayed to the user when the id was used in the URL. GET_DORM in dorm_controller.py is connected to the server with a dispatcher. 

###### run test_api.py

In order to run test_api.py, the server.py must be running as well. Simple type python3 into the command line and the output will show if any tests failed or if they were successful. 

### Rest API 

###### dorm_controller.py

The controller has event handler functions that are connected to the server through a dispatcher. These are the following handlers on the file:

GET_DORM - takes dorm's id and passes it to get_dorm method. It then extracts the information on that dorm from get_dorm and stores it in a dictionary. 

PUT_DORM - adds a new dorm by using the information given by PUT.

DELETE_DORM - takes in the dorm's id and uses the delete_dorm method to remove the dorm and information.

POST_INDEX - does not take the dorm's id as input. The new dorm is automatically appended to the end of the dorm dictionary.

GET_INDEX - uses for loop through all the dorms in the database to add to the dictionary.

DELETE_INDEX - uses for loop through all dorms in the database and deletes the dictionary.

###### server.py

The server is on port 51023. The file uses the cherrypy to connect to the server and the event handlers. Additionally, an object is created for the database to properly use methods in the class. The server uses HTTP requests GET, PUT, POST, and DELETE data from its stored dictionary. CORS is also added. 

###### test_ws.py

Tests the functionality of the event handlers as defined in the dorm controller. It performs tests by checking what the server outputs is the expected data. For example, The function loads the data and confirms if the index asked for is the correct data - in other words, if it matches with the indexing in the dataset. The function confirms the information using the dorm id = 19. The id = 19 is equal to Lyons Hall. The function then checks if the information provided by the server is equal to key indexed. The name, year, gender, quad, and mascot keys should return Lyons, 1927, Female, South, and Lion for the index 19 - respectively. 

###### run test_api.py

In order to run test_ws.py, the server.py must be running as well. Simple type python3 into the command line and the output will show if any tests failed or if they were successful. 

## Frontend

###### index.html

The site is very simple. At the very top, there is a short description of the motivation of the webpage. Then there is a picture of Notre Dame, After the picture, there are two buttons. The one on the left displays all the dorms. The one on the right clears the text from the button on the left. Further down, there are instructions on how the input is interpreted. After that, there is an input field where the user can input the dorm they want to learn more about. Thus, the buttons below become active and show the information regarding the categories on the dorm.dat file. The categories are: year established, gender housed, quad location, and mascot. Additionally, there is a button that will display all the information from the dorm. 

###### main.js

The main.js file manipulates the information displayed on the site based on the input of the user. It listens to the buttons clicked and, like described above, acts accordingly. It uses the function netcall_api() to extract information from the server.py. Based on the button clicked, the information is displayed in different ways. If the user wants to see all the dorms, the information extracted through the API will parse through the data and print out all the dorms. If the only female or male dorm button is clicked, then only the female or male dorms will show. If information is requested from only one dorm then the get_name() will get the input from the user. The dorm will then be passed to the network call API and and the information of that dorm will be extracted. If the dorm does not exist, then the responsive test will display that the dorm is not at Notre Dame. 

